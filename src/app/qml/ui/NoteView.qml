/*
 * Copyright: 2013 Canonical, Ltd
 *
 * This file is part of reminders
 *
 * reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Controls.Suru 2.2
import Ubuntu.Components 1.3
import QtWebEngine 1.7
import QtWebSockets 1.0
import Ubuntu.Content 1.0
import Evernote 0.1
import "../components"

Item {
    id: root
    property var note: null

    signal editNote()

    BouncingProgressBar {
        anchors.top: parent.top
        visible: root.note == null || root.note.loading
        z: 10
    }

    Component.onDestruction: {
        if (priv.dirty) {
            NotesStore.saveNote(note.guid);
        }
    }

    QtObject {
        id: priv
        property bool dirty: false
    }

    WebSocketServer {
        id: server
        listen: true
        port: NoteHelper.port

        onClientConnected: {
            console.log('Websocket server: connected');

            webSocket.onTextMessageReceived.connect(function(message) {
                console.log('Websocket server: message recieved', message);
                var data = JSON.parse(message);

                switch (data.type) {
                    case "checkboxChanged":
                        note.markTodo(data.todoId, data.checked);
                        priv.dirty = true;
                        break;
                    case "attachmentOpened":
                        var filePath = root.note.resource(data.resourceHash).hashedFilePath;
                        contentPeerPicker.filePath = filePath;

                        if (data.mediaType == "application/pdf") {
                            contentPeerPicker.contentType = ContentType.Documents;
                        } else if (data.mediaType.split("/")[0] == "audio" ) {
                            contentPeerPicker.contentType = ContentType.Music;
                        } else if (data.mediaType.split("/")[0] == "image" ) {
                            contentPeerPicker.contentType = ContentType.Pictures;
                        } else if (data.mediaType == "application/octet-stream" ) {
                            contentPeerPicker.contentType = ContentType.All;
                        } else {
                            contentPeerPicker.contentType = ContentType.Unknown;
                        }
                        contentPeerPicker.visible = true;
                    }
            });
        }

        onErrorStringChanged: {
            console.log('Websocket server: error', errorString);
        }
    }

    WebEngineView {
        id: noteTextArea
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        property string html: root.note ? note.htmlContent : ""
        url: 'file://' + NoteHelper.notePath

        onHtmlChanged: {
            NoteHelper.writeNote(html, Suru.backgroundColor, Suru.foregroundColor);
            noteTextArea.reload();
        }

        Connections {
            target: note ? note : null
            onResourcesChanged: {
                NoteHelper.writeNote(html, Suru.backgroundColor, Suru.foregroundColor);
                noteTextArea.reload();
            }
        }

        zoomFactor: 2.5 //scales the webpage on the device, range allowed from 0.25 to 5.0; the default factor is 1.0
    }

    ContentItem {
        id: exportItem
        name: i18n.tr("Attachment")
    }

    ContentPeerPicker {
        id: contentPeerPicker
        visible: false
        contentType: ContentType.Unknown
        handler: ContentHandler.Destination
        anchors.fill: parent

        property string filePath: ""
        onPeerSelected: {
            var transfer = peer.request();
            if (transfer.state === ContentTransfer.InProgress) {
                var items = new Array()
                var path = contentPeerPicker.filePath;
                exportItem.url = path
                items.push(exportItem);
                transfer.items = items;
                transfer.state = ContentTransfer.Charged;
            }
            contentPeerPicker.visible = false
        }
        onCancelPressed: contentPeerPicker.visible = false
    }
}
